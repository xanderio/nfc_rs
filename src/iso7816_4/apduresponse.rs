use crate::error::Error::{
    ApplicationNotFound, AuthenticationDelay, AuthenticationError, BoundaryError, CommandAborted,
    DuplicateError, FileNotFound, IllegalCommandCode, IntegrityError, InvalidStatusWord,
    LengthError, NoSuchKey, ParameterError, PermissionDenied,
};
use crate::error::Result;
use crate::iso7816_4::apdustatuswords::{APDUStatusWord, APDUStatusWord2};
use num_traits::FromPrimitive;
use std::fmt;

#[derive(Eq, PartialEq, Hash, Debug, Default)]
pub struct APDUResponse {
    pub body: Option<Vec<u8>>,
    pub sw1: u8,
    pub sw2: u8,
}

impl APDUResponse {
    pub fn body(&self) -> &Option<Vec<u8>> {
        &self.body
    }
}

impl From<APDUResponse> for Vec<u8> {
    fn from(resp: APDUResponse) -> Self {
        let mut v: Vec<u8> = vec![];
        match resp.body {
            None => {}
            Some(body) => {
                v.extend(body);
            }
        }
        v.push(resp.sw1);
        v.push(resp.sw2);
        v
    }
}

impl fmt::Display for APDUResponse {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.body {
            None => {
                write!(f, "SW1: {:#X} | SW2: {:#X}", self.sw1, self.sw2)
            }
            Some(body) => {
                write!(
                    f,
                    "SW1: {:#X} | SW2: {:#X} | Body: {:#X?}",
                    self.sw1, self.sw2, body
                )
            }
        }
    }
}
impl APDUResponse {
    pub fn new(raw: &[u8]) -> Self {
        if raw.len() < 2 {
            panic!("APDU response must be at least 2 bytes long");
        }
        if raw.len() == 2 {
            APDUResponse {
                body: None,
                sw1: raw[0],
                sw2: raw[1],
            }
        } else {
            APDUResponse {
                body: Some(raw[..raw.len() - 2].to_vec()),
                sw1: raw[raw.len() - 2],
                sw2: raw[raw.len() - 1],
            }
        }
    }

    pub fn get_statusword(&self) -> Result<APDUStatusWord> {
        match self.sw1 {
            0x61 => Ok(APDUStatusWord::DataReady),
            0x62 => Ok(APDUStatusWord::StorageNotChanged),
            0x63 => {
                if (self.sw2 & 0xF0) == 0xC0 {
                    Ok(APDUStatusWord::CounterReached)
                } else {
                    Ok(APDUStatusWord::StorageChanged)
                }
            }
            0x64 => Ok(APDUStatusWord::ExecutionErrorWithoutChange),
            0x65 => Ok(APDUStatusWord::ExecutionErrorWithChange),
            0x6C => Ok(APDUStatusWord::InvalidLe),
            _ => FromPrimitive::from_u16(((self.sw1 as u16) << 8) | self.sw2 as u16)
                .ok_or(InvalidStatusWord),
        }
    }

    pub fn get_data_length(&self) -> u8 {
        self.sw2
    }

    pub fn get_counter(&self) -> u8 {
        self.sw2 & 0x0F
    }

    pub fn get_correct_le(&self) -> u8 {
        self.sw2
    }

    pub fn check(&self) -> Result<()> {
        if self.sw1 == 0x91 {
            match FromPrimitive::from_u8(self.sw2) {
                Some(APDUStatusWord2::OperationOk) => Ok(()),
                Some(APDUStatusWord2::NoChanges) => Ok(()),
                Some(APDUStatusWord2::IllegalCommandCode) => Err(IllegalCommandCode),
                Some(APDUStatusWord2::IntegrityError) => Err(IntegrityError),
                Some(APDUStatusWord2::NoSuchKey) => Err(NoSuchKey),
                Some(APDUStatusWord2::LengthError) => Err(LengthError),
                Some(APDUStatusWord2::PermissionDenied) => Err(PermissionDenied),
                Some(APDUStatusWord2::ParameterError) => Err(ParameterError),
                Some(APDUStatusWord2::AuthenticationDelay) => Err(AuthenticationDelay),
                Some(APDUStatusWord2::AuthenticationError) => Err(AuthenticationError),
                Some(APDUStatusWord2::AdditionalFrame) => Ok(()),
                Some(APDUStatusWord2::BoundaryError) => Err(BoundaryError),
                Some(APDUStatusWord2::CommandAborted) => Err(CommandAborted),
                Some(APDUStatusWord2::DuplicateError) => Err(DuplicateError),
                Some(APDUStatusWord2::FileNotFound) => Err(FileNotFound),
                Some(APDUStatusWord2::ApplicationNotFound) => Err(ApplicationNotFound),
                None => Err(InvalidStatusWord),
            }
        } else {
            Err(InvalidStatusWord)
        }
    }
}
