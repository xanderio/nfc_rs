use num_derive::FromPrimitive;

#[derive(FromPrimitive)]
#[repr(u16)]
pub enum APDUStatusWord {
    /// Kommando erfolgreich ausgefhrt. xx Datenbytes knnen mit dem GET RESPONSE-Kommando abgeholt werden. Statuswort zur Steuerung des T=0-Protokolls
    DataReady = 0x6100,

    /// Die zurckgegebenen Daten knnen fehlerhaft sein.
    FaultyData = 0x6281,

    /// Da das Dateiende vorher erreicht wurde, konnten nur weniger als Le Bytes gelesen werden.
    UnexpectedEndOfFile = 0x6282,

    /// Die ausgewhlte Datei ist gesperrt (englisch invalidated, wrtlich ungltig).
    InvalidatedFile = 0x6283,

    /// Die File Control Information (FCI) ist inkonform zu ISO 7816-4.
    FciNotConform = 0x6284,

    /// Warnung; Zustand des nichtflchtigen Speichers nicht verndert
    StorageNotChanged = 0x6200,

    /// Zhler hat den Wert x erreicht (die genaue Bedeutung ist vom Kommando abhngig)
    CounterReached = 0x63C0,

    /// Warnung; Zustand des nichtflchtigen Speichers verndert
    StorageChanged = 0x6300,

    /// Ausfhrungsfehler; Zustand des nichtflchtigen Speichers nicht verndert
    ExecutionErrorWithoutChange = 0x6400,

    /// Speicherfehler
    MemoryError = 0x6581,

    /// Ausfhrungsfehler; Zustand des nichtflchtigen Speichers verndert
    ExecutionErrorWithChange = 0x6500,

    /// Befehlslnge (Lc) oder erwartete Antwortlnge (Le) falsch
    InvalidLcLe = 0x6700,

    /// Funktionen im Class-Byte werden nicht untersttzt
    ClassFeatureNotSupported = 0x6800,

    /// Logische Kanle werden nicht untersttzt
    LogicChannelNotSupported = 0x6881,

    /// Secure Messaging wird nicht untersttzt
    SecureMessagingNotSupported = 0x6882,

    /// Kommando nicht erlaubt
    CommandNotAllowed = 0x6900,

    /// Kommando inkompatibel zur Dateistruktur
    CommandIncompatible = 0x6981,

    /// Sicherheitszustand nicht erfllt
    SafetyStatusNotFulfilled = 0x6982,

    /// Authentisierungsmethode ist gesperrt
    AuthenticationMethodLocked = 0x6983,

    /// Referenzierte Daten sind gesperrt
    ReferencedFileLocked = 0x6984,

    /// Nutzungsbedingungen sind nicht erfllt
    TermsOfServiceNotFulfilled = 0x6985,

    /// Kommando nicht erlaubt (kein EF selektiert)
    CommandNotAllowedNoEfSelected = 0x6986,

    /// Erwartete Secure-Messaging-Objekte nicht gefunden
    ExpectedSecureMessagingObjectsNotFound = 0x6987,

    /// Secure-Messaging-Datenobjekte sind inkorrekt
    InvalidSecureMessagingObjects = 0x6988,

    /// Falsche Parameter P1/P2
    WrongParameters = 0x6A00,

    /// Falsche Daten
    WrongData = 0x6A80,

    /// Funktion wird nicht untersttzt
    FeatureNotSupported = 0x6A81,

    /// Datei wurde nicht gefunden
    FileNotFound = 0x6A82,

    /// Datensatz (engl. record) der Datei nicht gefunden
    RecordNotFound = 0x6A83,

    /// Nicht gengend Speicherplatz in der Datei
    InsufficientSpace = 0x6A84,

    /// Lc nicht konsistent mit der TLV-Struktur
    LcTlvInconsistent = 0x6A85,

    /// Inkorrekte Parameter P1/P2
    IncorrectParameters = 0x6A86,

    /// Lc inkonsistent mit P1/P2
    LcParametersInconsistent = 0x6A87,

    /// Referenzierte Daten nicht gefunden
    ReferencedFileNotFound = 0x6A88,

    /// Parameter P1/P2 falsch
    WrongParameters2 = 0x6B00,

    /// Falsche Lnge Le; xx gibt die korrekte Lnge an Statuswort zur Steuerung des T=0-Protokolls
    InvalidLe = 0x6C00,

    /// Das Kommando (INS) wird nicht untersttzt
    InstructionNotSupported = 0x6D00,

    /// Die Kommandoklasse (CLA) wird nicht untersttzt
    ClassNotSupported = 0x6E00,

    /// Kommando wurde mit unbekanntem Fehler abgebrochen
    UnknownError = 0x6F00,

    /// Kommando erfolgreich ausgefhrt
    SUCCESS = 0x9000,

    /// OK
    OK = 0x9100,
}

#[derive(FromPrimitive)]
#[repr(u8)]
pub enum APDUStatusWord2 {
    OperationOk = 0x00,
    NoChanges = 0x0C,
    IllegalCommandCode = 0x1C,
    IntegrityError = 0x1E,
    NoSuchKey = 0x40,
    LengthError = 0x7E,
    PermissionDenied = 0x9D,
    ParameterError = 0x9E,
    AuthenticationDelay = 0xAD,
    AuthenticationError = 0xAE,
    AdditionalFrame = 0xAF,
    BoundaryError = 0xBE,
    CommandAborted = 0xCA,
    DuplicateError = 0xDE,
    FileNotFound = 0xF0,
    ApplicationNotFound = 0xA0,
}
