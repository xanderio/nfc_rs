use num_derive::FromPrimitive;

/// Access Rights for changing application keys (Change Key command)
#[repr(u8)]
#[derive(Clone, Copy, FromPrimitive)]
pub enum ChangeApplicationKey {
    /// Application master key authentication is necessary to change any key (default)
    MASTERKEY = 0x00,
    /// Authentication with the key to be changed (same Key#) is necessary to change a key
    SAMEKEY = 0x0E,
    /// All keys (except application master key, see Bit 0) within this application are frozen
    ALLKEYS = 0x0F,
}

/// codes whether the application master key is changeable
#[repr(u8)]
#[derive(Clone, Copy)]
pub enum ChangeMasterKey {
    /// Application master key is not changeable anymore (frozen)
    FROZEN = 0x00,

    /// Application master key is changeable (authentication with the current application master key necessary, default)
    CHANGEABLE = 0x01,
}

/// codes whether a change of the application master key settings is allowed
#[repr(u8)]
#[derive(Clone, Copy)]
pub enum ChangeMasterKeySettings {
    /// configuration not changeable anymore (frozen)
    FROZEN = 0x00,

    /// this configuration is changeable if authenticated with the application master key (default)
    WITHMASTERKEY = 0x08,
}

/// codes whether application master key authentication is needed before “Create File” / “Delete File”
#[repr(u8)]
#[derive(Clone, Copy)]
pub enum CreateDeleteFile {
    /// “Create File”/ “Delete File”is permitted only with application master key authentication
    ONLYMASTERKEY = 0x00,

    /// “Create File”/ “Delete File”is permitted also without application master key authentication (default)
    NOKEY = 0x04,
}

/// Crypto method of the application
#[repr(u8)]
#[derive(Clone, Copy)]
pub enum CryptoOperationsType {
    TDES = 0x00,
    TKTDES = 0x40,
    AES = 0x80,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum FileAccessRights {
    FREE = 0x0E,
    NEVER = 0x0F,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum FileCommunication {
    /// "Plain communication"
    PLAIN = 0x00,

    /// Plain communication secured by DES/3DES MACing
    MAC = 0x01,

    /// Fully DES/3DES enciphered communication
    ENCRYPT = 0x03,
}

/// codes whether application master key authentication is needed for file directory access
#[repr(u8)]
#[derive(Clone, Copy)]
pub enum FileDirectoryAccess {
    /// Successful application master key authentication is required for executing
    /// the “Get FID List”, “Get File Settings”and “Get Key Settings”commands
    ONLYMASTERKEY = 0x00,

    /// “Get FID List”, “Get File Settings” and “Get Key Settings” commands succeed
    /// independently of a preceding application master key authentication (default)
    NOKEY = 0x02,
}

/// Indicates use of 2 byte ISO/IEC 7816-4 File Identifies for files within the Application
#[repr(u8)]
#[derive(Clone, Copy)]
pub enum FileIdentifiers {
    NOTUSED = 0x00,
    USED = 0x20,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum FileTypes {
    /// Standard Data File
    STANDARD = 0x00,

    /// Backup Data Files
    BACKUP = 0x01,

    /// Value Files with Backup
    VALUE = 0x02,

    /// Linear Record Files with Backup
    LINEARRECORD = 0x03,

    /// Cyclic Record Files with Backup
    CYCLICRECORD = 0x04,
}

mod apduinstructions;
mod apdustatuscodes;
#[allow(clippy::module_inception)]
pub mod desfire;
pub use crate::desfire::desfire::Desfire;
