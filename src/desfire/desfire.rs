use crate::crypto::cipher::aes::AES;
use crate::crypto::cipher::tdes::Tdes;
use crate::crypto::cipher::Cipher;
use crate::crypto::util;
use crate::crypto::util::expand_to_blocksize;
use crate::desfire::apduinstructions::APDUInstructions;
use crate::desfire::{
    ChangeApplicationKey, ChangeMasterKey, ChangeMasterKeySettings, CreateDeleteFile,
    CryptoOperationsType, FileCommunication, FileDirectoryAccess, FileIdentifiers,
};
use crate::error::Error::{
    InvalidApplicationID, InvalidFileID, InvalidKeyID, InvalidKeyVersion, InvalidLength, NumKeys,
};
use crate::error::{Error, Result};
use crate::iso7816_4::apducommand::{APDUCommand, IsoCase};
use crate::{error, Card};
use num_traits::FromPrimitive;

/// Maximum number of bytes in a single transaction while communicating with a nfc token
pub const MAX_BYTES_PER_TRANSACTION: usize = 47;

pub struct Desfire {
    pub card: Option<Box<dyn Card>>,
    pub session_key: Option<Vec<u8>>,
    pub cbc_iv: Option<Vec<u8>>,
}

impl Desfire {
    // Desfire commands

    pub fn select_application_cmd(&self, aid: u32) -> Result<APDUCommand> {
        if aid > 0xFFFFFF {
            return Err(InvalidApplicationID);
        }

        Ok(APDUCommand {
            case: IsoCase::Case4Short,
            cla: 0x90,
            ins: APDUInstructions::SelectApplication as u8,
            data: Option::from(aid.to_le_bytes()[..3].to_vec()),
            ..Default::default()
        })
    }

    /// Select Application by 3 Byte ApplicationID `aid`
    pub fn select_application(&self, aid: u32) -> Result<()> {
        let cmd_select_app = self.select_application_cmd(aid)?;

        let response = self.card.as_ref().unwrap().transmit(cmd_select_app)?;

        response.check()
    }

    pub fn get_application_ids_cmd(&self) -> Result<APDUCommand> {
        Ok(APDUCommand {
            case: IsoCase::Case2Short,
            cla: 0x90,
            ins: APDUInstructions::GetApplicationIds as u8,
            ..Default::default()
        })
    }

    /// Get all application ids on the defire token
    pub fn get_application_ids(&self) -> Result<Vec<u32>> {
        let cmd_get_application_ids = self.get_application_ids_cmd()?;

        let response = self
            .card
            .as_ref()
            .unwrap()
            .transmit(cmd_get_application_ids)?;
        response.check()?;

        if let Some(body) = response.body {
            Ok(body
                // application ids are 3 byte long MSB first values
                // TODO: use `slice::array_chunks` when its stable
                .chunks_exact(3)
                // pad with one byte extra byte to it it into an u32
                .map(|chunk| u32::from_le_bytes([chunk[0], chunk[1], chunk[2], 0x0]))
                .collect())
        } else {
            // body is empty, there are no applications on this card
            // except the picc 0x000000 application
            Ok(vec![])
        }
    }

    /// Authenticate to PICC, with ISO Authenticate for DES Key
    ///
    /// Arguments:
    /// * `key_id`: 0x01 - 0x0D
    /// * `key`: Array of 8/16 bytes
    /// * `rnd_a`: !!! WARNING for testing only !!!
    pub fn authenticate_iso_des(
        &mut self,
        key_id: u8,
        key: &[u8],
        rnd_a: Option<[u8; 8]>,
    ) -> Result<()> {
        if key_id > 0x0E {
            return Err(InvalidKeyID);
        }

        let cmd_challenge_request = APDUCommand {
            case: IsoCase::Case4Short,
            cla: 0x90,
            ins: APDUInstructions::AuthenticateIso as u8,
            data: Option::from(vec![key_id]),
            ..Default::default()
        };

        let response = self
            .card
            .as_ref()
            .unwrap()
            .transmit(cmd_challenge_request)
            .unwrap();

        match response.check() {
            Ok(_) => {}
            Err(e) => {
                return Err(e);
            }
        }

        let rnd_b_response_body = response.body.unwrap();
        let rnd_b_enc = rnd_b_response_body.as_slice();
        // println!("RND_B_ENC: {:x?}", rnd_b_enc);

        let rnd_b = Tdes::decrypt(rnd_b_enc, key, vec![0_u8; 8].as_slice()).unwrap();
        // println!("RND_B: {:x?}", rnd_b);

        let mut rnd_b_rl = rnd_b.clone();
        rnd_b_rl.rotate_left(1);
        // println!("RND_B_RL: {:x?}", rnd_b_rl);

        //FIXME: This is ugly, we should find a better way to make the function testable
        //TODO: Check if we need a CSPRNG here
        let rnd_a = match rnd_a {
            None => rand::random(),
            Some(i) => i,
        };
        // println!("RND_A: {:x?}", rnd_a);

        let rnd_ab = [&rnd_a, rnd_b_rl.as_slice()].concat();
        // println!("RND_AB: {:x?}", rnd_ab);

        let rnd_ab_enc = Tdes::encrypt(rnd_ab.as_slice(), key, rnd_b_enc).unwrap();
        // println!("RND_AB_ENC: {:x?}", rnd_ab_enc);

        let cmd_challenge_response = APDUCommand {
            case: IsoCase::Case4Short,
            cla: 0x90,
            ins: APDUInstructions::Continue as u8,
            data: Some(rnd_ab_enc.clone()),
            ..Default::default()
        };

        let response = self
            .card
            .as_ref()
            .unwrap()
            .transmit(cmd_challenge_response)
            .unwrap();

        match response.check() {
            Ok(_) => {}
            Err(e) => {
                return Err(e);
            }
        }

        let iv: &[u8] = util::extract_last_block(rnd_ab_enc.as_slice(), 8).unwrap();
        let rnd_a_enc_from_card = response.body.unwrap();
        // println!("RND_A_ENC_FROM_CARD: {:x?}", rnd_a_enc_from_card.as_slice());
        let mut rnd_a_rot_from_card =
            Tdes::decrypt(rnd_a_enc_from_card.as_slice(), key, iv).unwrap();
        // println!("RND_A_ROT_FROM_CARD: {:x?}", rnd_a_rot_from_card);
        rnd_a_rot_from_card.rotate_right(1);
        let rnd_a_from_card = rnd_a_rot_from_card.as_slice();
        // println!("RND_A_FROM_CARD: {:x?}", rnd_a_from_card);

        if rnd_a != rnd_a_from_card {
            return Err(Error::InvalidPICCChallenge);
        }

        self.session_key = Some(generate_session_key_des(&rnd_a, rnd_b.as_slice()).unwrap());
        // println!("SESSION_KEY: {:x?}", self.session_key.as_ref().unwrap());

        self.cbc_iv = Some(vec![0_u8; 8]);
        // println!("CBC_IV: {:x?}", self.cbc_iv.as_ref().unwrap());
        Ok(())
    }

    /// Request challenge for aes auth from card
    pub fn authenticate_iso_aes_challenge_cmd(&mut self, key_id: u8) -> Result<APDUCommand> {
        if key_id > 0x0E {
            return Err(InvalidKeyID);
        }

        let cmd_challenge_request = APDUCommand {
            case: IsoCase::Case4Short,
            cla: 0x90,
            ins: APDUInstructions::AuthenticateAes as u8,
            data: Option::from(vec![key_id]),
            ..Default::default()
        };

        Ok(cmd_challenge_request)
    }

    /// Generate response for aes auth challenge
    pub fn authenticate_iso_aes_response_cmd(
        &mut self,
        challenge: &[u8],
        key: &[u8],
        rnd_a: &[u8],
    ) -> Result<(APDUCommand, Vec<u8>, Vec<u8>)> {
        // println!("RND_B_ENC: {:x?}", challenge);

        let rnd_b = AES::decrypt(challenge, key, vec![0_u8; 16].as_slice()).unwrap();
        // println!("RND_B: {:x?}", rnd_b);

        // auth_iv = rnd_b.clone();
        let mut rnd_b_rl = rnd_b.clone();
        rnd_b_rl.rotate_left(1);
        // println!("RND_B_RL: {:x?}", rnd_b_rl);

        let rnd_ab = [rnd_a, rnd_b_rl.as_slice()].concat();
        // println!("RND_AB: {:x?}", rnd_ab);

        let rnd_ab_enc = AES::encrypt(rnd_ab.as_slice(), key, challenge).unwrap();
        // println!("RND_AB_ENC: {:x?}", rnd_ab_enc);

        let iv: &[u8] = util::extract_last_block(rnd_ab_enc.as_slice(), 16)?;

        let cmd_challenge_response = APDUCommand {
            case: IsoCase::Case4Short,
            cla: 0x90,
            ins: APDUInstructions::Continue as u8,
            data: Some(rnd_ab_enc.clone()),
            ..Default::default()
        };

        Ok((cmd_challenge_response, rnd_b, iv.to_vec()))
    }

    /// Verify response from card
    pub fn authenticate_iso_aes_verify(
        &mut self,
        response: &[u8],
        expected_response: &[u8],
        challenge: &[u8],
        key: &[u8],
        iv: &[u8],
    ) -> Result<()> {
        let mut rnd_a_rot_from_card = AES::decrypt(response, key, iv)?;
        rnd_a_rot_from_card.rotate_right(1);
        // println!("RND_A_ROT_FROM_CARD: {:x?}", rnd_a_rot_from_card);
        let rnd_a_from_card = rnd_a_rot_from_card.as_slice();
        // println!("RND_A_FROM_CARD: {:x?}", rnd_a_from_card);

        if expected_response != rnd_a_from_card {
            return Err(Error::InvalidPICCChallenge);
        }

        self.session_key = Some(generate_session_key_aes(expected_response, challenge).unwrap());
        // println!("SESSION_KEY: {:x?}", self.session_key.as_ref().unwrap());

        self.cbc_iv = Some(vec![0_u8; 16]); //FIXME: this should be a random value
                                            // println!("CBC_IV: {:x?}", self.cbc_iv.as_ref().unwrap());

        Ok(())
    }

    /// Authenticate to PICC, with ISO Authenticate for AES Key
    ///
    /// # Parameters
    /// * `key_id`: 0x01 - 0x0D
    /// * `key`: Array of 8/16 bytes
    /// * `rnd_a`: !!! WARNING for testing only !!!
    pub fn authenticate_iso_aes(
        &mut self,
        key_id: u8,
        key: &[u8],
        rnd_a: Option<[u8; 16]>,
    ) -> Result<()> {
        let cmd_challenge_request = self.authenticate_iso_aes_challenge_cmd(key_id)?;
        // println!("CMD_CHALLENGE_REQUEST: {}", cmd_challenge_request);

        let response = self
            .card
            .as_ref()
            .unwrap()
            .transmit(cmd_challenge_request)?;

        match response.check() {
            Ok(_) => {}
            Err(e) => {
                return Err(e);
            }
        }
        // println!("RESPONSE: {}", response);

        let rnd_b_response_body = response.body.unwrap();
        let rnd_b_enc = rnd_b_response_body.as_slice();

        //FIXME: This is ugly, we should find a better way to make the function testable
        //TODO: Check if we need a CSPRNG here
        let rnd_a = match rnd_a {
            None => rand::random(),
            Some(i) => i,
        };
        // println!("RND_A: {:x?}", rnd_a);

        let (cmd_challenge_response, rnd_b, iv) =
            self.authenticate_iso_aes_response_cmd(rnd_b_enc, key, &rnd_a)?;

        let response = self
            .card
            .as_ref()
            .unwrap()
            .transmit(cmd_challenge_response)?;

        match response.check() {
            Ok(_) => {}
            Err(e) => {
                return Err(e);
            }
        }
        // println!("RESPONSE: {}", response);

        let rnd_a_enc_from_card = response.body.unwrap();
        // println!("RND_A_ENC_FROM_CARD: {:x?}", rnd_a_enc_from_card.as_slice());
        self.authenticate_iso_aes_verify(
            rnd_a_enc_from_card.as_slice(),
            rnd_a.as_slice(),
            rnd_b.as_slice(),
            key,
            iv.as_slice(),
        )
    }

    /// Format PICC, this is akin to a factory reset
    ///
    /// # Note
    /// This function will fail if you haven't authenticated against the PICC (Application ID: 0)
    pub fn format_picc(&self) -> Result<()> {
        let cmd_format = APDUCommand {
            case: IsoCase::Case2Short,
            cla: 0x90,
            ins: APDUInstructions::FormatPicc as u8,
            ..Default::default()
        };

        let response = self.card.as_ref().unwrap().transmit(cmd_format).unwrap();

        response.check()
    }

    /// Create a new application with `aid` as the application ID
    ///
    /// # Arguments
    /// * `aid`: 3 byte ID
    /// * `keysetting1`: TODO
    /// * `keysetting2`: TODO
    pub fn create_application(&self, aid: u32, keysetting1: u8, keysetting2: u8) -> Result<()> {
        if aid > 0xFFFFFF {
            return Err(InvalidApplicationID);
        }

        let mut data = aid.to_le_bytes()[..3].to_vec();
        data.push(keysetting1);
        data.push(keysetting2);
        let cmd_create_application = APDUCommand {
            case: IsoCase::Case4Short,
            cla: 0x90,
            ins: APDUInstructions::CreateApplication as u8,
            data: Option::from(data), //FIXME: Which byteorder?
            ..Default::default()
        };

        let response = self
            .card
            .as_ref()
            .unwrap()
            .transmit(cmd_create_application)
            .unwrap();

        response.check()
    }

    /// Change AES key, that is currently authenticated
    ///
    /// # Notes
    ///
    /// This will fail if the key you are trying to change isn't authenticated
    ///
    /// # Arguments
    /// * `key_id` - 0x01 - 0x0D
    /// * `new_key` - Array of 16 Bytes
    /// * `key_version` - Version of Key(min. 0x10)
    ///
    /// Also see: [`Desfire::change_other_key_aes`]
    pub fn change_key_aes(&mut self, key_id: u8, new_key: &[u8], key_version: u8) -> Result<()> {
        if key_id >= 0x0E {
            return Err(InvalidKeyID);
        }
        if key_version < 0x10 {
            return Err(InvalidKeyVersion);
        }

        let header = vec![0xC4, key_id];
        let key_and_version: Vec<u8> = [new_key, &[key_version]].concat();
        let mut command = vec![];

        command.extend(&header);
        command.extend(&key_and_version);
        // println!("HEADER: {:x?}", header);
        // println!("COMMAND: {:x?}", command);

        let crc = crate::crypto::crc::crc32::calculate(command.as_slice());
        // println!("CRC: {:x?}", crc);

        let mut plaintext: Vec<u8> = vec![];
        plaintext.extend(key_and_version);
        plaintext.extend(crc);
        // println!("PLAINTEXT: {:x?}", plaintext);

        let plaintext_pad = expand_to_blocksize(plaintext.as_mut_slice(), 16)?;
        // println!("PLAINTEXT_PAD: {:x?}", plaintext_pad);

        let cryptogram = AES::encrypt(
            plaintext_pad.as_slice(),
            self.session_key.as_ref().unwrap(),
            self.cbc_iv.as_ref().unwrap(),
        )?;
        // println!("CRYPTOGRAM: {:x?}", cryptogram);

        self.cbc_iv = Some(util::extract_last_block(cryptogram.as_slice(), 16)?.to_vec());
        // println!("CBC_IV: {:x?}", self.cbc_iv.as_ref().unwrap());

        let mut data: Vec<u8> = vec![key_id];
        data.extend(cryptogram);
        // println!("DATA: {:x?}", data);

        let cmd_change_key = APDUCommand {
            case: IsoCase::Case4Short,
            cla: 0x90,
            ins: APDUInstructions::ChangeKey as u8,
            data: Option::from(data), //FIXME: Which byteorder?
            ..Default::default()
        };
        // println!("CMD_CHANGE_KEY: {}", cmd_change_key);

        let response = self
            .card
            .as_ref()
            .unwrap()
            .transmit(cmd_change_key)
            .unwrap();
        // println!("RESPONSE: {}", response);

        response.check()
    }

    /// Change AES key, of a currently not authenticated key
    ///
    /// # Arguments:
    /// * `key_id` - 0x01 - 0x0D
    /// * `new_key` - Array of 16 Bytes
    /// * `old_key` - Array of 16 Bytes
    /// * `key_version` - Version of Key(min. 0x10)
    ///
    /// Also see: [`Desfire::change_key_aes`]
    pub fn change_other_key_aes(
        &mut self,
        key_id: u8,
        new_key: &[u8],
        old_key: &[u8],
        key_version: u8,
    ) -> Result<()> {
        if key_id >= 0x0E {
            return Err(InvalidKeyID);
        }
        if key_version < 0x10 {
            return Err(InvalidKeyVersion);
        }

        let header = vec![0xC4, key_id];
        // println!("HEADER: {:x?}", header);

        let key_xor: Vec<u8> = new_key
            .iter()
            .zip(old_key.iter())
            .map(|(&x1, &x2)| x1 ^ x2)
            .collect();
        // println!("KEY_XOR: {:x?}", key_xor);

        let key_and_version: Vec<u8> = [key_xor, vec![key_version]].concat();
        // println!("KEY_AND_VERSION: {:x?}", key_and_version);
        let mut command = vec![];

        command.extend(&header);
        command.extend(&key_and_version);
        // println!("COMMAND: {:x?}", command);

        let crc_cmd = crate::crypto::crc::crc32::calculate(command.as_slice());
        // println!("CRC_CMD: {:x?}", crc_cmd);

        let crc_key = crate::crypto::crc::crc32::calculate(new_key);
        // println!("CRC_KEY: {:x?}", crc_key);

        let mut plaintext: Vec<u8> = vec![];
        plaintext.extend(key_and_version);
        plaintext.extend(crc_cmd);
        plaintext.extend(crc_key);
        // println!("PLAINTEXT: {:x?}", plaintext);

        let plaintext_pad = expand_to_blocksize(plaintext.as_mut_slice(), 16)?;
        // println!("PLAINTEXT_PAD: {:x?}", plaintext_pad);

        let cryptogram = AES::encrypt(
            plaintext_pad.as_slice(),
            self.session_key.as_ref().unwrap(),
            self.cbc_iv.as_ref().unwrap(),
        )?;
        // println!("CRYPTOGRAM: {:x?}", cryptogram);

        self.cbc_iv = Some(util::extract_last_block(cryptogram.as_slice(), 16)?.to_vec());
        // println!("CBC_IV: {:x?}", self.cbc_iv.as_ref().unwrap());

        let mut data: Vec<u8> = vec![key_id];
        data.extend(cryptogram);
        // println!("DATA: {:x?}", data);

        let cmd_change_key = APDUCommand {
            case: IsoCase::Case4Short,
            cla: 0x90,
            ins: APDUInstructions::ChangeKey as u8,
            data: Option::from(data), //FIXME: Which byteorder?
            ..Default::default()
        };
        // println!("CMD_CHANGE_KEY: {}", cmd_change_key);

        let response = self
            .card
            .as_ref()
            .unwrap()
            .transmit(cmd_change_key)
            .unwrap();
        // println!("RESPONSE: {}", response);

        response.check()
    }

    /// Create a new standard file
    ///
    /// # Arguments
    /// * `file_id` - ID of the new file (0x00 - 0x20)
    /// * `communication` - TODO
    /// * `access_rights` - TODO
    /// * `size` - size of the file in bytes
    pub fn create_file_standard(
        &self,
        file_id: u8,
        communication: FileCommunication,
        access_rights: u16,
        size: u32,
    ) -> Result<()> {
        if file_id >= 0x20 {
            return Err(InvalidFileID);
        }

        let access_rights = access_rights.to_le_bytes().to_vec();
        let size = size.to_le_bytes()[..3].to_vec();
        let mut data = vec![file_id, communication as u8];
        data.extend(&access_rights);
        data.extend(&size);

        let cmd_create_file_standard = APDUCommand {
            case: IsoCase::Case4Short,
            cla: 0x90,
            ins: APDUInstructions::CreateStddatafile as u8,
            data: Option::from(data),
            ..Default::default()
        };
        // println!("CMD_CREATE_FILE_STANDARD: {}", cmd_create_file_standard);

        let response = self
            .card
            .as_ref()
            .unwrap()
            .transmit(cmd_create_file_standard)
            .unwrap();
        // println!("RESPONSE: {}", response);

        response.check()
    }

    /// Write Data to File
    ///
    /// # Parameters
    /// * `file_id` - Id of the file (0x00 - 0x20)
    /// * `offset` - offset of the file
    /// * `data` - data to write
    pub fn write_data(&self, file_id: u8, offset: u32, data: &[u8]) -> Result<()> {
        if file_id >= 0x20 {
            return Err(InvalidFileID);
        }

        // println!("Writing data to file {}", file_id);

        let mut bytes_writen: usize = 0;
        let length: usize = data.len();

        let mut ret: Result<()> = Err(error::Error::Simple(simple_error::simple_error!(
            "Error while writing data to card"
        ))); //TODO: Replace with better error

        while bytes_writen != data.len() {
            let bytes_towrite = match length - bytes_writen < MAX_BYTES_PER_TRANSACTION {
                true => length - bytes_writen,
                false => MAX_BYTES_PER_TRANSACTION,
            };

            let mut write_buffer = vec![file_id];
            write_buffer.append(&mut (offset as usize + bytes_writen).to_le_bytes()[..3].to_vec());
            write_buffer.append(&mut bytes_towrite.to_le_bytes()[..3].to_vec());
            write_buffer.append(&mut data[bytes_writen..bytes_writen + bytes_towrite].to_vec());
            bytes_writen += bytes_towrite;

            // println!("WRITE_BUFFER: {:x?}", write_buffer);
            // println!("BYTES_WRITEN: {}", bytes_writen);

            let cmd_write_data = APDUCommand {
                case: IsoCase::Case4Short,
                cla: 0x90,
                ins: APDUInstructions::WriteData as u8,
                data: Option::from(write_buffer),
                ..Default::default()
            };
            // println!("CMD_WRITE_DATA: {}", cmd_write_data);

            let response = self
                .card
                .as_ref()
                .unwrap()
                .transmit(cmd_write_data)
                .unwrap();
            // println!("RESPONSE: {}", response);

            ret = response.check();
        }

        ret
    }

    pub fn read_data_chunk_cmd(
        &self,
        file_id: u8,
        offset: u32,
        length: usize,
    ) -> Result<APDUCommand> {
        if file_id >= 0x20 {
            return Err(InvalidFileID);
        }

        if length > MAX_BYTES_PER_TRANSACTION {
            return Err(InvalidLength);
        }

        let mut sendbuf = vec![file_id];
        sendbuf.append(&mut offset.to_le_bytes()[..3].to_vec());
        sendbuf.append(&mut length.to_le_bytes()[..3].to_vec());

        let cmd_read_data_chunk = APDUCommand {
            case: IsoCase::Case4Short,
            cla: 0x90,
            ins: APDUInstructions::ReadData as u8,
            data: Option::from(sendbuf),
            ..Default::default()
        };

        Ok(cmd_read_data_chunk)
    }

    /// Read data from a file
    ///
    /// # Note
    /// This operation will fail if you try to read more data then there is in the file
    ///
    /// # Parameters
    /// * `file_id` - ID of the file (0x00 - 0x20)
    /// * `offset` - Offset of the file
    /// * `length` - lenght of the data to read
    pub fn read_data(&self, file_id: u8, offset: u32, length: usize) -> Result<Vec<u8>> {
        if file_id >= 0x20 {
            return Err(InvalidFileID);
        }

        let mut bytes_read: usize = 0;

        let mut read_buffer: Vec<u8> = vec![];

        while bytes_read != length {
            let bytes_toread = match length - bytes_read < MAX_BYTES_PER_TRANSACTION {
                true => length - bytes_read,
                false => MAX_BYTES_PER_TRANSACTION,
            };

            let cmd_read_data = self
                .read_data_chunk_cmd(file_id, (offset as usize + bytes_read) as u32, bytes_toread)
                .unwrap();
            // println!("CMD_READ_DATA: {}", cmd_read_data);

            bytes_read += bytes_toread;

            let response = self.card.as_ref().unwrap().transmit(cmd_read_data).unwrap();
            // println!("RESPONSE: {}", response);

            response.check()?;
            // // println!("RESPONSE_DATA: {:x?}, WITHOUT_CMAC: {:x?}", response.body.as_ref().unwrap(), response.body.as_ref().unwrap()[..bytes_toread].to_vec());

            read_buffer.append(&mut response.body.unwrap()[..bytes_toread].to_vec());
        }

        Ok(read_buffer)
    }
}

/// Generates SessionKey for DES Authentification
///
/// returns 8Byte SessionKey
fn generate_session_key_des(rnd_a: &[u8], rnd_b: &[u8]) -> Option<Vec<u8>> {
    let sessionkey = [&rnd_a[..4], &rnd_b[..4]].concat();
    Some([sessionkey.as_slice(), sessionkey.as_slice()].concat())
}

/// Generates SessionKey for AES Authentification
///
/// returns 16Byte SessionKey
fn generate_session_key_aes(rnd_a: &[u8], rnd_b: &[u8]) -> Option<Vec<u8>> {
    //FIXME: Check math
    Some([&rnd_a[..4], &rnd_b[..4], &rnd_a[12..], &rnd_b[12..]].concat())
}

/// Generate KeySetting1 for Application Settings or PICC Setting
///
/// # Parameters
/// * `change_key` - ID of the key for changing application keys
/// * `change_master_key` - TODO
/// * `create_delete_file` - TODO
/// * `file_directory_access` - TODO
/// * `change_master_key` - TODO
pub fn generate_keysetting1(
    change_key: u8,
    change_masterkey_settings: ChangeMasterKeySettings,
    create_delete_file: CreateDeleteFile,
    file_directory_access: FileDirectoryAccess,
    change_master_key: ChangeMasterKey,
) -> Result<u8> {
    match FromPrimitive::from_u8(change_key) {
        Some(ChangeApplicationKey::MASTERKEY)
        | Some(ChangeApplicationKey::SAMEKEY)
        | Some(ChangeApplicationKey::ALLKEYS) => Ok((change_key << 4)
            | change_masterkey_settings as u8
            | create_delete_file as u8
            | file_directory_access as u8
            | change_master_key as u8),
        None => {
            if !(0x01..0x08).contains(&change_key) {
                Err(InvalidKeyID)
            } else {
                Ok((change_key << 4)
                    | change_masterkey_settings as u8
                    | create_delete_file as u8
                    | file_directory_access as u8
                    | change_master_key as u8)
            }
        }
    }
}

/// Generate KeySetting2 for Application Creation
///
/// # Parameters
/// * `crypto_operations` - TODO
/// * `file_identifier` - TODO
/// * `num_keys` - Number of keys that can be stored within the application (0x01 - 0x0D)
pub fn generate_keysetting2(
    crypto_operations: CryptoOperationsType,
    file_identifier: FileIdentifiers,
    num_keys: u8,
) -> Result<u8> {
    if !(0x01..0x0D).contains(&num_keys) {
        Err(NumKeys)
    } else {
        Ok(crypto_operations as u8 | file_identifier as u8 | num_keys)
    }
}

/// Generate FileAccess Rights for File Settings
///
/// # Note
/// Use [`FileAccessRights`] for Free or Never Option
///
/// # Parameters
/// * `read` key_id for read access
/// * `write` key_id for write access
/// * `read_write` key_id for read and write access
/// * `configure` key_id for configuration access
///
/// [`FileAccessRights`]: crate::desfire::FileAccessRights
pub fn generate_file_access_rights(
    read: u8,
    write: u8,
    read_write: u8,
    configure: u8,
) -> Result<u16> {
    if read > 0x0F || write >= 0x0F || read_write >= 0x0F || configure >= 0x0F {
        Err(InvalidKeyID)
    } else {
        Ok((((read as u16) << 12)
            | ((write as u16) << 8)
            | ((read_write as u16) << 4)
            | configure as u16) as u16)
    }
}

#[cfg(test)]
mod tests {
    use crate::crypto::cipher_key::CipherKey;
    use crate::crypto::cipher_type::CipherType;
    use crate::desfire::desfire::{
        generate_file_access_rights, generate_keysetting1, generate_keysetting2,
        generate_session_key_aes, generate_session_key_des, Desfire,
    };

    use crate::desfire::{
        ChangeApplicationKey, ChangeMasterKey, ChangeMasterKeySettings, CreateDeleteFile,
        CryptoOperationsType, FileAccessRights, FileCommunication, FileDirectoryAccess,
        FileIdentifiers,
    };
    use crate::error::Error::CardError;
    use crate::error::Result;

    use crate::{APDUCommand, APDUResponse, Card as CardTrait};
    use hex_literal::hex;
    use mockall::mock;
    use pcsc::{Card, Context, Protocols, Scope, ShareMode, MAX_BUFFER_SIZE};
    use std::convert::TryFrom;
    use std::ffi::CString;

    mock! {
        pub VirtualCard {}
        impl CardTrait for VirtualCard {
            fn connect(&mut self) -> Result<()>;
            fn disconnect(&mut self) -> Result<()>;
            fn transmit(&self, apdu_cmd: APDUCommand) -> Result<APDUResponse>;
        }
    }

    pub struct PCSCCard {
        ctx: Context,
        reader: CString,
        card: Option<Card>,
    }

    impl CardTrait for PCSCCard {
        fn connect(&mut self) -> Result<()> {
            self.card = match self
                .ctx
                .connect(&self.reader, ShareMode::Shared, Protocols::ANY)
            {
                Ok(card) => Some(card),
                Err(_err) => {
                    // eprintln!("Failed to connect to card: {}", err);
                    return Err(CardError);
                }
            };

            return Ok(());
        }

        fn disconnect(&mut self) -> Result<()> {
            Ok(())
        }

        fn transmit(&self, apdu_cmd: APDUCommand) -> Result<APDUResponse> {
            // println!("{}", apdu_cmd);
            let apdu = Vec::<u8>::try_from(apdu_cmd).unwrap();
            let mut rapdu_buf = [0; MAX_BUFFER_SIZE];
            let rapdu = match self
                .card
                .as_ref()
                .as_ref()
                .unwrap()
                .transmit(apdu.as_slice(), &mut rapdu_buf)
            {
                Ok(rapdu) => rapdu,
                Err(_err) => {
                    // eprintln!("Failed to transmit APDU command to card: {}", err);
                    return Err(CardError);
                }
            };
            return Ok(APDUResponse::new(rapdu));
        }
    }

    #[test]
    fn generate_sessionkey_des() {
        let rnd_a = hex!("a541a9dc9138df07");
        let rnd_b = hex!("cbe55aa893b2da25");

        let expected_sessionkey = hex!("a541a9dccbe55aa8a541a9dccbe55aa8");

        let sessionkey = generate_session_key_des(&rnd_a, &rnd_b).unwrap();

        // println!("expected sessionkey: {:X?}", expected_sessionkey);
        // println!("actual sessionkey:   {:X?}", sessionkey.as_slice());

        assert_eq!(expected_sessionkey, sessionkey.as_slice());
    }

    #[test]
    fn generate_sessionkey_aes() {
        let rnd_a = hex!("bc14dfde20074617e45a8822f06fdd91");
        let rnd_b = hex!("482ddc54426e6dee560413b8d95471f5");

        let expected_sessionkey = hex!("bc14dfde482ddc54f06fdd91d95471f5");

        let sessionkey = generate_session_key_aes(&rnd_a, &rnd_b).unwrap();

        // println!("expected sessionkey: {:X?}", expected_sessionkey);
        // println!("actual sessionkey:   {:X?}", sessionkey.as_slice());

        assert_eq!(expected_sessionkey, sessionkey.as_slice());
    }

    #[test]
    fn generate_key_setting1() {
        assert_eq!(
            0x0B,
            generate_keysetting1(
                ChangeApplicationKey::MASTERKEY as u8,
                ChangeMasterKeySettings::WITHMASTERKEY,
                CreateDeleteFile::ONLYMASTERKEY,
                FileDirectoryAccess::NOKEY,
                ChangeMasterKey::CHANGEABLE
            )
            .unwrap()
        )
    }

    #[test]
    fn generate_key_setting1_changekey() {
        assert_eq!(
            0x1B,
            generate_keysetting1(
                0x01,
                ChangeMasterKeySettings::WITHMASTERKEY,
                CreateDeleteFile::ONLYMASTERKEY,
                FileDirectoryAccess::NOKEY,
                ChangeMasterKey::CHANGEABLE
            )
            .unwrap()
        )
    }

    #[test]
    #[should_panic(expected = "InvalidKeyID")]
    fn generate_key_setting1_invalid_keyid() {
        generate_keysetting1(
            0x10,
            ChangeMasterKeySettings::WITHMASTERKEY,
            CreateDeleteFile::ONLYMASTERKEY,
            FileDirectoryAccess::NOKEY,
            ChangeMasterKey::CHANGEABLE,
        )
        .unwrap();
    }

    #[test]
    fn generate_key_setting2() {
        assert_eq!(
            0x82,
            generate_keysetting2(CryptoOperationsType::AES, FileIdentifiers::NOTUSED, 0x02)
                .unwrap()
        );
    }

    #[test]
    #[should_panic(expected = "NumKeys")]
    fn generate_key_setting2_wrong_num_keys() {
        generate_keysetting2(CryptoOperationsType::AES, FileIdentifiers::NOTUSED, 0x10).unwrap();
    }

    #[test]
    fn generate_file_accessrights() {
        assert_eq!(
            0x1234,
            generate_file_access_rights(0x01, 0x02, 0x03, 0x04).unwrap()
        )
    }

    #[test]
    #[should_panic(expected = "InvalidKeyID")]
    fn generate_file_accessrights_outofrange() {
        generate_file_access_rights(0x10, 0x00, 0x00, 0x00).unwrap();
    }

    #[test]
    fn select_application() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap())
                    == vec![0x90, 0x5a, 0x00, 0x00, 0x03, 0x33, 0x22, 0x11, 0x00]
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    body: None,
                    sw1: 0x91,
                    sw2: 0x00,
                })
            });

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        assert!(desfire.select_application(0x112233).is_ok())
    }

    #[test]
    #[should_panic(expected = "InvalidApplicationID")]
    fn select_application_invalidaid() {
        let mock = MockVirtualCard::new();

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        desfire.select_application(0xff000000).unwrap();
    }

    #[test]
    fn get_application_ids() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap()) == vec![0x90, 0x6a, 0x00, 0x00, 0x00]
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    body: Some(vec![0x01, 0x00, 0x00, 0x02, 0x00, 0x00]),
                    sw1: 0x91,
                    sw2: 0x00,
                })
            });

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        let ids = desfire.get_application_ids().unwrap();
        assert_eq!(ids, vec![1, 2])
    }

    #[test]
    #[ignore]
    fn select_application_hardware() {
        // Establish a PC/SC context.
        let ctx = match Context::establish(Scope::User) {
            Ok(ctx) => ctx,
            Err(_err) => {
                // eprintln!("Failed to establish context: {}", err);
                std::process::exit(1);
            }
        };

        // List available readers.
        let mut readers_buf = [0; 2048];
        let mut readers = match ctx.list_readers(&mut readers_buf) {
            Ok(readers) => readers,
            Err(_err) => {
                // eprintln!("Failed to list readers: {}", err);
                std::process::exit(1);
            }
        };

        // Use the first reader.
        let reader = match readers.next() {
            Some(reader) => reader,
            None => {
                // println!("No readers are connected.");
                return;
            }
        };

        let mut card = PCSCCard {
            ctx,
            reader: CString::from(reader),
            card: None,
        };

        card.connect();

        let desfire = Desfire {
            card: Some(Box::new(card)),
            cbc_iv: None,
            session_key: None,
        };

        assert!(desfire.select_application(0x000000).is_ok());
    }

    #[test]
    fn authenticate_des() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap())
                    == vec![0x90, 0x1a, 0x00, 0x00, 0x01, 0x00, 0x00]
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    sw1: 0x91,
                    sw2: 0xAF,
                    body: Some(vec![0x2b, 0xf9, 0xa9, 0x38, 0xec, 0xca, 0x02, 0xe2]),
                })
            });

        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap())
                    == hex!("90af000010f8cdb2eaa42a3167dfcb53852ce267fd00")
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    sw1: 0x91,
                    sw2: 0x00,
                    body: Some(vec![0x07, 0xd8, 0x25, 0x60, 0x7a, 0x55, 0x2e, 0x2e]),
                })
            });

        let rnd_a = hex!("5f7d1dd12d979173");
        let key = CipherKey::new_empty(CipherType::TDES).unwrap();
        // println!("{:x?}", key.key.deref());

        let mut desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        desfire
            .authenticate_iso_des(0x00, key.key.as_ref(), Some(rnd_a))
            .unwrap();

        let sessionkey_expected = hex!("5f7d1dd1f449db5c5f7d1dd1f449db5c");
        let iv_expected = hex!("0000000000000000");

        assert_eq!(desfire.session_key.unwrap(), sessionkey_expected);
        assert_eq!(desfire.cbc_iv.unwrap(), iv_expected);
    }

    #[test]
    #[should_panic(expected = "InvalidKeyID")]
    fn authenticate_des_invalid_keyno() {
        let mock = MockVirtualCard::new();

        let mut desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        desfire
            .authenticate_iso_des(0x0F, &[0x00 as u8], None)
            .unwrap();
    }

    #[test]
    #[ignore]
    fn authenticate_des_hardware() {
        // Establish a PC/SC context.
        let ctx = match Context::establish(Scope::User) {
            Ok(ctx) => ctx,
            Err(_err) => {
                // eprintln!("Failed to establish context: {}", err);
                std::process::exit(1);
            }
        };

        // List available readers.
        let mut readers_buf = [0; 2048];
        let mut readers = match ctx.list_readers(&mut readers_buf) {
            Ok(readers) => readers,
            Err(_err) => {
                // eprintln!("Failed to list readers: {}", err);
                std::process::exit(1);
            }
        };

        // Use the first reader.
        let reader = match readers.next() {
            Some(reader) => reader,
            None => {
                // println!("No readers are connected.");
                return;
            }
        };

        let mut card = PCSCCard {
            ctx,
            reader: CString::from(reader),
            card: None,
        };

        card.connect();

        let mut desfire = Desfire {
            card: Some(Box::new(card)),
            cbc_iv: None,
            session_key: None,
        };

        desfire.select_application(0x000000);

        let key = CipherKey::new_empty(CipherType::TDES).unwrap();
        match desfire.authenticate_iso_des(0x00, key.key.as_ref(), None) {
            Ok(_) => {}
            Err(_err) => {
                // eprintln!("Failed to authenticate: {}", err);
                panic!("Failed to authenticate");
            }
        }
    }

    #[test]
    fn authenticate_aes() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90aa0000010000")
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    sw1: 0x91,
                    sw2: 0xAF,
                    body: Some(hex!("a33856932308775cf464610c2b17a558").to_vec()),
                })
            });

        mock.expect_transmit().withf(|x: &APDUCommand| (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90af000020cbe9726faf54bc76b2055d0b9700e7dc97ecad5627f1d1702a16e8408d2a0ada00")).return_once(move |_| Ok(APDUResponse{
            sw1: 0x91,
            sw2: 0x00,
            body: Some(hex!("8fdc476f6bac44fe9150e285abd68d48").to_vec())
        }));

        let rnd_a = hex!("2176770e7a6eb4bef00d5e4b201d1e57");
        let key = CipherKey::new_empty(CipherType::AES).unwrap();
        // println!("{:x?}", key.key.deref());

        let mut desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        desfire
            .authenticate_iso_aes(0x00, key.key.as_ref(), Some(rnd_a))
            .unwrap();

        let sessionkey_expected = hex!("2176770e11c512ca201d1e57fde6e15a");
        let iv_expected = hex!("00000000000000000000000000000000");

        assert_eq!(desfire.session_key.unwrap(), sessionkey_expected);
        assert_eq!(desfire.cbc_iv.unwrap(), iv_expected);
    }

    #[test]
    #[should_panic(expected = "InvalidKeyID")]
    fn authenticate_aes_invalid_keyno() {
        let mock = MockVirtualCard::new();

        let mut desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        desfire
            .authenticate_iso_aes(0x0F, &[0x00 as u8], None)
            .unwrap();
    }

    #[test]
    fn format_picc() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90fc000000")
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    sw1: 0x91,
                    sw2: 0x00,
                    body: None,
                })
            });

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        assert!(desfire.format_picc().is_ok());
    }

    #[test]
    fn create_application() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90ca000005eeffaa0b8200")
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    sw1: 0x91,
                    sw2: 0x00,
                    body: None,
                })
            });

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        assert!(desfire.create_application(0xAAFFEE, 0x0b, 0x82).is_ok());
    }

    #[test]
    #[should_panic(expected = "InvalidApplicationID")]
    fn create_application_invalid_aid() {
        let mock = MockVirtualCard::new();

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        desfire.create_application(0xFF000000, 0x0b, 0x82).unwrap();
    }

    #[test]
    fn change_key_aes() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit().withf(|x: &APDUCommand| (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90c400002100c2b54a718d0251845653199909bb32e8e38bd6719e8dc21799c29c922a0984fc00")).return_once(move |_| Ok(APDUResponse {
            sw1: 0x91,
            sw2: 0x00,
            body: None
        }));

        let new_key = hex!("25432a462d4a614e645267556b587032");

        let mut desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: Some(hex!("00000000000000000000000000000000").to_vec()),
            session_key: Some(hex!("a8514dd0350f3dfbc86e80744bcc9b57").to_vec()),
        };

        assert!(desfire.change_key_aes(0x00, new_key.as_ref(), 0x10).is_ok());
    }

    #[test]
    #[should_panic(expected = "InvalidKeyID")]
    fn change_key_aes_invalid_keyno() {
        let mock = MockVirtualCard::new();

        let mut desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        desfire.change_key_aes(0x0F, &[0x00 as u8], 0x10).unwrap();
    }

    #[test]
    fn change_other_key_aes() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit().withf(|x: &APDUCommand| (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90c400002101a8c5a61a06f56f38dc91266fed2e87dc00a5ad72a634ff0e62c8d6d80707dd6000")).return_once(move |_| Ok(APDUResponse {
            sw1: 0x91,
            sw2: 0x00,
            body: None
        }));

        let old_key = hex!("00000000000000000000000000000000");
        let new_key = hex!("25432a462d4a614e645267556b587032");

        let mut desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: Some(hex!("00000000000000000000000000000000").to_vec()),
            session_key: Some(hex!("1677623e1e158a62dc3d128db55f947d").to_vec()),
        };

        assert!(desfire
            .change_other_key_aes(0x01, new_key.as_ref(), old_key.as_ref(), 0x10)
            .is_ok());
    }

    #[test]
    #[should_panic(expected = "InvalidKeyID")]
    fn change_other_key_aes_invalid_keyno() {
        let mock = MockVirtualCard::new();

        let mut desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        desfire
            .change_other_key_aes(0x0F, &[0x00 as u8], &[0x00 as u8], 0x10)
            .unwrap();
    }

    #[test]
    fn create_file_standard() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90cd000007010000e0f0000000")
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    sw1: 0x91,
                    sw2: 0x00,
                    body: None,
                })
            });

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        let access_rights =
            generate_file_access_rights(FileAccessRights::FREE as u8, 0x00, 0x00, 0x00).unwrap();
        assert!(desfire
            .create_file_standard(0x01, FileCommunication::PLAIN, access_rights, 0xF0)
            .is_ok());
    }

    #[test]
    #[should_panic(expected = "InvalidFileID")]
    fn create_file_standard_invalid_file_id() {
        let mock = MockVirtualCard::new();

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        let access_rights =
            generate_file_access_rights(FileAccessRights::FREE as u8, 0x00, 0x00, 0x00).unwrap();
        desfire
            .create_file_standard(0xFF, FileCommunication::PLAIN, access_rights, 0xF0)
            .unwrap();
    }

    #[test]
    fn write_data() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap())
                    == hex!("903d00000f01000000080000546573743132333400")
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    sw1: 0x91,
                    sw2: 0x00,
                    body: None,
                })
            });

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        let data = "Test1234".as_bytes();
        assert!(desfire.write_data(0x01, 0x00, data.as_ref()).is_ok());
    }

    #[test]
    fn write_data_long() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit().withf(|x: &APDUCommand| (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("903d000036010000002f0000546573743132333454657374313233345465737431323334546573743132333454657374313233345465737431323300")).return_once(move |_| Ok(APDUResponse {
            sw1: 0x91,
            sw2: 0x00,
            body: None
        }));

        mock.expect_transmit().withf(|x: &APDUCommand| (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("903d000036012f00002f0000345465737431323334546573743132333454657374313233345465737431323334546573743132333454657374313200")).return_once(move |_| Ok(APDUResponse {
            sw1: 0x91,
            sw2: 0x00,
            body: None
        }));

        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap())
                    == hex!("903d000019015e000012000033345465737431323334546573743132333400")
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    sw1: 0x91,
                    sw2: 0x00,
                    body: None,
                })
            });

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        let data = "Test1234Test1234Test1234Test1234Test1234Test1234Test1234Test1234Test1234Test1234Test1234Test1234Test1234Test1234".as_bytes();
        assert!(desfire.write_data(0x01, 0x00, data.as_ref()).is_ok());
    }

    #[test]
    #[should_panic(expected = "InvalidFileID")]
    fn write_data_invalid_file_id() {
        let mock = MockVirtualCard::new();

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        desfire.write_data(0xFF, 0x00, &[0 as u8; 1]).unwrap();
    }

    #[test]
    fn read_data() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90bd0000070100000020000000")
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    sw1: 0x91,
                    sw2: 0x00,
                    body: Some(
                        hex!(
                            "54657374313233340000000000000000000000000000000000000000000000009100"
                        )
                        .to_vec(),
                    ),
                })
            });

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        let data = desfire.read_data(0x01, 0x00, 0x20).unwrap();
        assert_eq!(
            "Test1234",
            String::from_utf8(data).unwrap().replace("\0", "")
        );
    }

    #[test]
    fn read_data_cmac() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit().withf(|x: &APDUCommand| (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90bd0000070100000020000000")).return_once(move |_| Ok(APDUResponse {
            sw1: 0x91,
            sw2: 0x00,
            body: Some(hex!("5465737431323334000000000000000000000000000000000000000000000000809a9bedbc559a5b9100").to_vec())
        }));

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        let data = desfire.read_data(0x01, 0x00, 0x20).unwrap();
        assert_eq!(
            "Test1234",
            String::from_utf8(data).unwrap().replace("\0", "")
        );
    }

    #[test]
    fn read_data_long() {
        let mut mock = MockVirtualCard::new();
        mock.expect_transmit().withf(|x: &APDUCommand| (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90bd000007010000002f000000")).return_once(move |_| Ok(APDUResponse {
            sw1: 0x91,
            sw2: 0x00,
            body: Some(hex!("54657374313233340000000000000000000000000000000000000000000000000000000000000000000000000000009100").to_vec())
        }));

        mock.expect_transmit().withf(|x: &APDUCommand| (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90bd000007012f00002f000000")).return_once(move |_| Ok(APDUResponse {
            sw1: 0x91,
            sw2: 0x00,
            body: Some(hex!("00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009100").to_vec())
        }));

        mock.expect_transmit()
            .withf(|x: &APDUCommand| {
                (Vec::<u8>::try_from(x.clone()).unwrap()) == hex!("90bd000007015e000002000000")
            })
            .return_once(move |_| {
                Ok(APDUResponse {
                    sw1: 0x91,
                    sw2: 0x00,
                    body: Some(hex!("00009100").to_vec()),
                })
            });

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        let data = desfire.read_data(0x01, 0x00, 0x60).unwrap();
        assert_eq!(
            "Test1234",
            String::from_utf8(data).unwrap().replace("\0", "")
        );
    }

    #[test]
    #[should_panic(expected = "InvalidFileID")]
    fn read_data_invalid_file_id() {
        let mock = MockVirtualCard::new();

        let desfire = Desfire {
            card: Some(Box::new(mock)),
            cbc_iv: None,
            session_key: None,
        };

        desfire.read_data(0xFF, 0x00, 0x20).unwrap();
    }
}
