#[allow(dead_code)]
#[repr(u8)]
pub enum APDUInstructions {
    AuthenticateIso = 0x1A,
    AuthenticateAes = 0xAA,
    ChangeKeySettings = 0x54,
    SetConfiguration = 0x5C,
    ChangeKey = 0xC4,
    GetKeyVersion = 0x64,
    CreateApplication = 0xCA,
    DeleteApplication = 0xDA,
    GetApplicationIds = 0x6A,
    FreeMemory = 0x6E,
    GetDfNames = 0x6D,
    GetKeySettings = 0x45,
    SelectApplication = 0x5A,
    FormatPicc = 0xFC,
    GetVersion = 0x60,
    GetCardUid = 0x51,
    GetFileIds = 0x6F,
    GetFileSettings = 0xF5,
    ChangeFileSettings = 0x5F,
    CreateStddatafile = 0xCD,
    CreateBackupdatafile = 0xCB,
    CreateValueFile = 0xCC,
    CreateLinearRecordFile = 0xC1,
    CreateCyclicRecordFile = 0xC0,
    DeleteFile = 0xDF,
    GetIsoFileIds = 0x61,
    ReadData = 0xBD,
    WriteData = 0x3D,
    GetValue = 0x6C,
    Credit = 0x0C,
    Debit = 0xDC,
    LimitedCredit = 0x1C,
    WriteRecord = 0x3B,
    ReadRecords = 0xBB,
    ClearRecordFile = 0xEB,
    CommitTransaction = 0xC7,
    AbortTransaction = 0xA7,
    Continue = 0xAF,
}
