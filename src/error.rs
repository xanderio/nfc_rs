use std::fmt;
use std::io;

#[derive(Debug)]
pub enum Error {
    IO(io::Error),
    Simple(simple_error::SimpleError),
    BlockModeError(block_modes::BlockModeError),
    InvalidKeyIvLength(block_modes::InvalidKeyIvLength),
    FromHexError(hex::FromHexError),
    PadError(block_modes::block_padding::PadError),
    InvalidStatusWord,
    IllegalCommandCode,
    IntegrityError,
    NoSuchKey,
    LengthError,
    PermissionDenied,
    ParameterError,
    AuthenticationDelay,
    AuthenticationError,
    BoundaryError,
    CommandAborted,
    DuplicateError,
    FileNotFound,
    InvalidApplicationID,
    ApplicationNotFound,
    InvalidAPDUResponse,
    InvalidIsoCase,
    InvalidKeyID,
    InvalidPICCChallenge,
    InvalidFileID,
    InvalidKeyVersion,
    InvalidLength,
    NumKeys,
    CardError,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::IO(e) => {
                write!(f, "IO Error: {}", e)
            }
            Error::Simple(e) => {
                write!(f, "Generic Error: {}", e)
            }
            Error::BlockModeError(e) => {
                write!(f, "CBC Error: {}", e)
            }
            Error::InvalidKeyIvLength(e) => {
                write!(f, "CBC InvalidKeyIvLength Error: {}", e)
            }
            Error::FromHexError(e) => {
                write!(f, "Hex conversion Error: {}", e)
            }
            Error::PadError(e) => {
                write!(f, "Padding to blocksize failed: {:#?}", e)
            }
            Error::InvalidStatusWord => {
                write!(f, "Invalid APDU Statusword")
            }
            Error::IllegalCommandCode => {
                write!(f, "SW2: Command code not supported.")
            }
            Error::IntegrityError => {
                write!(
                    f,
                    "SW2: CRC or MAC does not match data. Paddingbytes not valid."
                )
            }
            Error::NoSuchKey => {
                write!(f, "SW2: Invalid key number specified.")
            }
            Error::LengthError => {
                write!(f, "SW2: Length of command string invalid.")
            }
            Error::PermissionDenied => {
                write!(
                    f,
                    "SW2: Current configuration / status does not allow the requested command."
                )
            }
            Error::ParameterError => {
                write!(f, "SW2: Value of the parameter(s) invalid.")
            }
            Error::AuthenticationDelay => {
                write!(f, "SW2: Currently not allowed to authenticate. Keeptrying until full delay is spent.")
            }
            Error::AuthenticationError => {
                write!(
                    f,
                    "SW2: Current authentication status does not allow the requested command."
                )
            }
            Error::BoundaryError => {
                write!(f, "SW2: Attempt to read/write data from/to beyond the file’s/record’s limits. Attempt to exceed the limits of a value file.")
            }
            Error::CommandAborted => {
                write!(f, "SW2: Previous Command was not fully completed.Not all Frames were requested or provided by the PCD.")
            }
            Error::DuplicateError => {
                write!(f, "SW2: Creation of file/application failed because file/application with same number already exists")
            }
            Error::FileNotFound => {
                write!(f, "SW2: Specified file number does not exist.")
            }
            Error::InvalidApplicationID => {
                write!(f, "Application ID was larger then 24 bit")
            }
            Error::InvalidAPDUResponse => {
                write!(f, "Got malformed APDUResponse")
            }
            Error::InvalidIsoCase => {
                write!(f, "The specified IsoCase is not supported")
            }
            Error::InvalidKeyID => {
                write!(f, "Invalid KeyID: Was larger then 0x0E")
            }
            Error::InvalidKeyVersion => {
                write!(f, "Invalid KeyVersion: Should be atleast 0x10")
            }
            Error::InvalidPICCChallenge => {
                write!(f, "Authentication failed, PICC Challenge is invalid.")
            }
            Error::InvalidFileID => {
                write!(f, "Invalid FileID: Was larger then 0x20")
            }
            Error::NumKeys => {
                write!(f, "Number of Keys was larger then 0x0D")
            }
            Error::CardError => {
                write!(f, "An error occurred during communication with the card.")
            }
            Error::ApplicationNotFound => {
                write!(f, "The application was not found on the card.")
            }
            Error::InvalidLength => {
                write!(
                    f,
                    "More data was requested than can be handled in a single transaction."
                )
            }
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::IO(e) => Some(e),
            Error::FromHexError(e) => Some(e),
            Error::PadError(_) => None,
            Error::InvalidStatusWord => None,
            Error::IllegalCommandCode => None,
            Error::IntegrityError => None,
            Error::NoSuchKey => None,
            Error::LengthError => None,
            Error::PermissionDenied => None,
            Error::ParameterError => None,
            Error::AuthenticationDelay => None,
            Error::AuthenticationError => None,
            Error::BoundaryError => None,
            Error::CommandAborted => None,
            Error::DuplicateError => None,
            Error::FileNotFound => None,
            Error::InvalidApplicationID => None,
            Error::InvalidAPDUResponse => None,
            Error::InvalidIsoCase => None,
            Error::InvalidKeyID => None,
            Error::InvalidKeyVersion => None,
            Error::InvalidPICCChallenge => None,
            Error::InvalidFileID => None,
            Error::NumKeys => None,
            Error::CardError => None,
            Error::ApplicationNotFound => None,
            Error::InvalidLength => None,
            Error::Simple(e) => Some(e),
            Error::BlockModeError(e) => Some(e),
            Error::InvalidKeyIvLength(e) => Some(e),
        }
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::IO(e)
    }
}

impl From<simple_error::SimpleError> for Error {
    fn from(e: simple_error::SimpleError) -> Error {
        Error::Simple(e)
    }
}

impl From<block_modes::BlockModeError> for Error {
    fn from(e: block_modes::BlockModeError) -> Error {
        Error::BlockModeError(e)
    }
}

impl From<block_modes::InvalidKeyIvLength> for Error {
    fn from(e: block_modes::InvalidKeyIvLength) -> Error {
        Error::InvalidKeyIvLength(e)
    }
}

impl From<hex::FromHexError> for Error {
    fn from(e: hex::FromHexError) -> Error {
        Error::FromHexError(e)
    }
}

impl From<block_modes::block_padding::PadError> for Error {
    fn from(e: block_modes::block_padding::PadError) -> Error {
        Error::PadError(e)
    }
}

pub(crate) type Result<T> = std::result::Result<T, Error>;
