//! # desfire
//!
//! **WARNING** This is alpha quality software at best and should not be relied upon. **WARNING**
//!
//! The `desfire` Crate provides the cryptographic methods and specific commands for interfacing
//! with NXP MiFare Desfire cards.
//!

use crate::iso7816_4::apducommand::APDUCommand;
use crate::iso7816_4::apduresponse::APDUResponse;
use error::Result;

pub mod crypto;
pub mod desfire;
pub mod error;
pub mod iso7816_4;

pub trait Card {
    /// Connect to Smartcard
    fn connect(&mut self) -> Result<()>;

    /// Disconnect from Smartcard
    fn disconnect(&mut self) -> Result<()>;

    /// Transmit APDU Command to Smartcard
    ///
    /// # Parameters
    /// * `apdu_cmd` - Application Protocol Data Unit Command - ISO 7816
    ///
    /// returns a Application Protocol Data Unit Response - ISO 7816
    fn transmit(&self, apdu_cmd: APDUCommand) -> Result<APDUResponse>;
}
