use std::slice::from_ref;

const POLY: u32 = 0xEDB88320;
const INIT_VALUE: u32 = 0xFFFFFFFF;

pub fn calculate_with_initial(data: &[u8], mut crc32: u32) -> u32 {
    for b in data {
        crc32 ^= *b as u32;
        for _ in 0..8 {
            let b_bit = (crc32 & 0x01) > 0;
            crc32 >>= 1;
            if b_bit {
                crc32 ^= POLY;
            }
        }
    }

    crc32
}

pub fn calculate(data: &[u8]) -> Vec<u8> {
    let mut crc32 = INIT_VALUE;

    for d in data {
        crc32 = calculate_with_initial(from_ref(d), crc32);
    }

    crc32.to_ne_bytes().to_vec()
}

#[cfg(test)]
mod tests {
    use hex_literal::hex;

    #[test]
    fn calculate() {
        let data = hex!("c40045eeb8338ae8f49a032e85bb1114353010");
        let crc_expected = hex!("95c3894b");

        let crc = crate::crypto::crc::crc32::calculate(&data);

        assert_eq!(*crc, crc_expected);
    }
}
